package com.studioinc.sdk.consent;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.ads.consent.ConsentInformation;
import com.google.ads.consent.ConsentStatus;
import com.google.ads.consent.listener.enums.ConsentFormClosedStatus;
import com.google.ads.consent.listener.enums.ContentRating;
import com.google.ads.consent.ui.ConsentDialog;
import com.google.ads.consent.ui.ConsentManager;
import com.medmobdev.consentmanager.R;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, Listerner{
    private static String TAG = MainActivity.class.getName();
    private Button btnShowConsentInWebView, btnShowConsentInDiaog, btnResetConsent;
    private ConsentManager consentManager;
    private ConsentDialog consentDialog;
    private Listerner listerner;
    ContentRating contentRating;

    public void addListerner(Listerner lr){
        this.listerner = lr;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btnShowConsentInWebView = (Button) findViewById(R.id.btn_show_consent_in_WebView);
        btnShowConsentInDiaog = (Button) findViewById(R.id.btn_show_consent_in_dialog);
        btnResetConsent = (Button) findViewById(R.id.btn_reset_consent);


        btnShowConsentInWebView.setOnClickListener(this);
        btnShowConsentInDiaog.setOnClickListener(this);
        btnResetConsent.setOnClickListener(this);
        addListerner(this);
        consentManager = InitialConsent.getInstance(this);
        consentDialog = InitialConsent.getInstanceConsentDialog(this);
        formClosed();

        contentRating = ContentRating.MA;


        //Toast.makeText(this, contentRating.toString()+"", Toast.LENGTH_LONG).show();

    }

    private void formClosed(){
        if (consentManager.isConsentFormClosed() || consentDialog.isConsentFormClosed()){
            btnShowConsentInWebView.setVisibility(View.GONE);
            btnShowConsentInDiaog.setVisibility(View.GONE);
            btnResetConsent.setVisibility(View.VISIBLE);
        } else {
            btnShowConsentInWebView.setVisibility(View.VISIBLE);
            btnShowConsentInDiaog.setVisibility(View.VISIBLE);
            btnResetConsent.setVisibility(View.GONE);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_show_consent_in_WebView:
                startActivity(new Intent(MainActivity.this, ConsentActivity.class));
                break;
            case R.id.btn_show_consent_in_dialog:
                startActivity(new Intent(MainActivity.this, DialogActivity.class));
                break;
            case R.id.btn_reset_consent:
                listerner.onResetConsent();
                break;
        }
    }

    @Override
    public void onResetConsent() {
        //Toast.makeText(this, "", Toast.LENGTH_LONG).show();
        ConsentInformation.getInstance(this).setConsentStatus(ConsentStatus.UNKNOWN, "form");
        consentManager.setConsentFormClosed(ConsentFormClosedStatus.SET_RESET_CONSENT_FORM);
        consentDialog.setConsentFormClosed(ConsentFormClosedStatus.SET_RESET_CONSENT_FORM);
        formClosed();
    }
}
