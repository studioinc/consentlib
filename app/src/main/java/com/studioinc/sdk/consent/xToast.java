package com.studioinc.sdk.consent;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.widget.Toast;

public class xToast {
    private Context context;
    private String text;
    private int duration = Toast.LENGTH_SHORT;
    private Toast toast;

    public xToast(Context context) {
        this.context = context;
    }

    /**
     * Construct an empty Toast object.  You must call {@link #setView} before you
     * can call {@link #show}.
     *
     * @param context The context to use.  Usually your {@link Application}
     *                or {@link Activity} object.
     */



    public xToast(Context context, String text) {
        this.context = context;
        this.text = text;
    }

    public xToast(Context context, String text, int duration) {
        this.context = context;
        this.text = text;
        this.duration = duration;
    }

    public xToast show(){
        Toast.makeText(context, text, duration).show();
        return new xToast(context);
    }


}
