package com.studioinc.sdk.consent;

import android.content.Context;

import com.google.ads.consent.annotations.ContentRating;
import com.google.ads.consent.ui.ConsentDialog;
import com.google.ads.consent.ui.ConsentManager;
import com.google.android.gms.ads.AdRequest;
import com.medmobdev.consentmanager.R;


/**
 * Created by NoThing on 6/9/2018.
 */

public class InitialConsent {
    public static ConsentManager getInstance(Context context){
        return new ConsentManager.Builder()
                .context(context)
                .setPrivacyUrl(context.getResources().getString(R.string.privacy_url)) // Add your privacy policy url
                .setPublisherId(context.getResources().getString(R.string.admob_publisher_id)) // Add your admob publisher id
                .setTestDeviceId(AdRequest.DEVICE_ID_EMULATOR) // Add your test device id "Remove addTestDeviceId on production!"
                .setLog("CUSTOM_TAG") // Add custom tag default: ID_LOG
                .setDebugGeography(true) // Geography appears as in EEA for test devices.
                .setTagForChildDirectedTreatment(true)
                .build();
    }

    public static ConsentDialog getInstanceConsentDialog(Context context){
        return new ConsentDialog.Builder()
                .context(context)
                .setAppName(context.getString(R.string.app_name))
                .setPrivacyUrl(context.getResources().getString(R.string.privacy_url))
                .setPublisherId(context.getResources().getString(R.string.admob_publisher_id))
                .setDebugGeography(true)
                .setDialogFullscreen(false)
                .setMaxAdContentRating("G")
                .setTagForUnderAgeOfConsent(false)
                .listener(null)
                .build();
    }
}
