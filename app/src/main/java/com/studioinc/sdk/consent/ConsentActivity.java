package com.studioinc.sdk.consent;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.ads.consent.ConsentStatus;
import com.google.ads.consent.listener.abstracts.CustomAdListener;
import com.google.ads.consent.listener.enums.CustomAdSize;
import com.google.ads.consent.listener.abstracts.ConsentCallback;
import com.google.ads.consent.listener.interfaces.AdRewardedListener;
import com.google.ads.consent.util.AdManager;
import com.google.ads.consent.ui.ConsentManager;
import com.google.ads.consent.widget.CustomAdView;
import com.google.android.gms.ads.reward.RewardItem;
import com.medmobdev.consentmanager.R;

public class ConsentActivity extends AppCompatActivity {
    private ConsentManager consentManager;
    private AdManager adManager;
    private CustomAdView customAdView;
    private TextView txtStatus;
    private Button btnLoadBannerAd;
    private Button btnLoadInterstitlaAd;
    private Button btnLoadRewardedVideoAd;


    public class ConsentResultListener extends ConsentCallback {
        @Override
        public void onResult(boolean isRequestLocationInEeaOrUnknown, ConsentStatus consentStatus) {
            super.onResult(isRequestLocationInEeaOrUnknown, consentStatus);
            loadAd();
            txtStatus.setText(consentStatus.toString());
        }
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_consent);
        customAdView = (CustomAdView) findViewById(R.id.custom_adView);
        btnLoadBannerAd = (Button) findViewById(R.id.btn_load_banner);
        btnLoadInterstitlaAd = (Button) findViewById(R.id.btn_load_interstitial);
        btnLoadRewardedVideoAd = (Button) findViewById(R.id.btn_load_rewarded_video);
        txtStatus = (TextView) findViewById(R.id.txt_status);


        consentManager = InitialConsent.getInstance(this);
        consentManager.checkLocation(new ConsentResultListener());
        adManager = new AdManager(consentManager, true);


        btnLoadBannerAd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                adManager.loadAdView(customAdView, new AdListener());
            }
        });

        btnLoadInterstitlaAd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                adManager.loadAdInterstitial();
            }
        });

        btnLoadRewardedVideoAd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                adManager.loadAdRewardedVideo();
            }
        });

    }


    public void loadAd() {
        adManager.setAdAppId(null);
        adManager.setAdInterstitialId(null);
        adManager.setAdRewardedVideoId(null);
        adManager.setAdViewSize(CustomAdSize.SMART_BANNER);

        btnLoadBannerAd.setVisibility(View.VISIBLE);
        btnLoadInterstitlaAd.setVisibility(View.VISIBLE);
        btnLoadRewardedVideoAd.setVisibility(View.VISIBLE);
    }

    private class AdListener extends CustomAdListener {
        @Override
        public void onAdLoaded () {
            super.onAdLoaded();
            new xToast(ConsentActivity.this, "AdView nAdFailedToLoad").show();
        }

        @Override
        public void onAdFailedToLoad ( int var1){
            super.onAdFailedToLoad(var1);
            new xToast(ConsentActivity.this, "AdView onAdFailedToLoad").show();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        adManager.pause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        adManager.resume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        adManager.destroy();
    }

}
