package com.studioinc.sdk.consent;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.ads.consent.ConsentStatus;
import com.google.ads.consent.listener.abstracts.CustomAdListener;
import com.google.ads.consent.listener.enums.CustomAdSize;
import com.google.ads.consent.listener.abstracts.ConsentListenerCallback;
import com.google.ads.consent.listener.interfaces.AdRewardedListener;
import com.google.ads.consent.util.AdManager;
import com.google.ads.consent.ui.ConsentDialog;
import com.google.ads.consent.widget.CustomAdView;
import com.google.android.gms.ads.reward.RewardItem;
import com.medmobdev.consentmanager.R;

public class DialogActivity extends AppCompatActivity implements ConsentListenerCallback{
    private ConsentDialog consentDialog;
    private AdManager adManager;
    private CustomAdView customAdView;
    private TextView txtStatus;
    private Button btnLoadBannerAd;
    private Button btnLoadInterstitlaAd;
    private Button btnLoadRewardedVideoAd;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dialog);
        customAdView = (CustomAdView) findViewById(R.id.custom_adView);
        btnLoadBannerAd = (Button) findViewById(R.id.btn_load_banner);
        btnLoadInterstitlaAd = (Button) findViewById(R.id.btn_load_interstitial);
        btnLoadRewardedVideoAd = (Button) findViewById(R.id.btn_load_rewarded_video);
        txtStatus = (TextView) findViewById(R.id.txt_status);

        consentDialog = InitialConsent.getInstanceConsentDialog(this);
        consentDialog.show();
        consentDialog.setConsentCallback(this);
        adManager = new AdManager(consentDialog);


        btnLoadBannerAd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                adManager.loadAdView(customAdView, new AdListener());
            }
        });

        btnLoadInterstitlaAd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                adManager.loadAdInterstitialAfterCountOf(1);
            }
        });

        btnLoadRewardedVideoAd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                adManager.loadAdRewardedVideoAfterCountOf(1);
            }
        });
    }


    public void loadAd() {
        adManager.setAdAppId(null);
        adManager.setAdInterstitialId(null);
        adManager.setAdRewardedVideoId(null);
        adManager.setAdViewSize(CustomAdSize.SMART_BANNER);

        btnLoadBannerAd.setVisibility(View.VISIBLE);
        btnLoadInterstitlaAd.setVisibility(View.VISIBLE);
        btnLoadRewardedVideoAd.setVisibility(View.VISIBLE);
    }

    private class AdListener extends CustomAdListener {
        @Override
        public void onAdLoaded () {
            super.onAdLoaded();
            new xToast(DialogActivity.this, "AdView nAdFailedToLoad").show();
        }

        @Override
        public void onAdFailedToLoad ( int var1){
            super.onAdFailedToLoad(var1);
            new xToast(DialogActivity.this, "AdView onAdFailedToLoad").show();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        adManager.pause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        adManager.resume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        adManager.destroy();
    }

    @Override
    public void setResult() {

    }

    @Override
    public void setResultWithStatus(ConsentStatus consentStatus) {
        loadAd();
        txtStatus.setText(consentStatus.toString());
    }
}
