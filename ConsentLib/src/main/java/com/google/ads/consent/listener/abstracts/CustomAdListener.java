package com.google.ads.consent.listener.abstracts;

public abstract class CustomAdListener {

    public CustomAdListener() {
    }

    public void onAdClosed() {
    }

    public void onAdFailedToLoad(int var1) {
    }

    public void onAdLeftApplication() {
    }

    public void onAdOpened() {
    }

    public void onAdLoaded() {
    }

    public void onAdClicked() {
    }

    public void onAdImpression() {
    }
}
