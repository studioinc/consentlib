package com.google.ads.consent.widget;

import android.view.View;

public interface StyleHolder<V extends View> {
    void applyStyle(V view);
}