package com.google.ads.consent.ui;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;

import com.google.ads.consent.ConsentForm;
import com.google.ads.consent.ConsentInformation;
import com.google.ads.consent.annotations.ContentRating;
import com.google.ads.consent.listener.enums.CustomContentRating;
import com.google.ads.consent.util.ConsentPreferences;
import com.google.ads.consent.ConsentStatus;
import com.google.ads.consent.DebugGeography;
import com.google.ads.consent.listener.enums.ConsentFormClosedStatus;
import com.google.ads.consent.listener.abstracts.ConsentFormListener;
import com.google.ads.consent.listener.abstracts.ConsentCallback;
import com.google.ads.consent.listener.abstracts.ConsentInformationCallback;
import com.google.ads.consent.listener.abstracts.ConsentStatusCallback;
import com.google.ads.consent.listener.abstracts.LocationIsEeaOrUnknownCallback;
import com.google.ads.consent.listener.interfaces.ConsentInfoUpdateListener;
import com.google.ads.consent.util.Constants;
import com.google.ads.mediation.admob.AdMobAdapter;
import com.google.android.gms.ads.AdRequest;

import java.net.MalformedURLException;
import java.net.URL;

public class ConsentManager {
    public Context mContext;
    private ConsentForm form;
    public static ConsentManager consentManager;
    public ConsentPreferences consentPreferences;
    public static SharedPreferences sharedPreferences;


    public String  maxAdContentRating;
    public boolean tagForUnderAgeOfConsent;
    public boolean tagForChildDirecredTreatment;


    public static String adTestAppUnitId = "ca-app-pub-3940256099942544~3347511713";
    public static String adTestBannerId = "ca-app-pub-3940256099942544/6300978111";
    public static String adTestIntetstitiaId = "ca-app-pub-3940256099942544/1033173712";
    public static String adTestRewardedVideoId = "ca-app-pub-3940256099942544/5224354917";

    public static String ads_preference = "ads_preference";
    public static String form_closed_preference = "consent_form_closed_preference";
    public static String preferences_name = "preferences_name";
    public static String user_status = "user_status";
    public static String nothins = "nothins_value";


    public static boolean IS_PERSONALIZED = true;
    public static boolean NON_PERSONALIZED = false;
    public static boolean IS_DEBUG_GEOGRAPHY = false;

    public static boolean isAdsPersonalize;
    public static boolean isAdsNonPersonalize;
    public static boolean isUserNotFromEeaPersonalize;


    public static final int AdsNonPersonalize = 0;
    public static final int AdsPersonalize = 1;
    public static final int AdsUserNotFromEeaPersonalize = 2;


    public static String testDeviceId = "";
    public static String logTag = "";
    public static int adCountFullScreen = 0;
    public static int adCountRewardedVideo = 0;


    public static String adAppUnitId;
    public static String adBannerId;
    public static String adIntetstitiaId;
    public static String adRewardedVideoId;

    public static String privacyUrl;
    public static String publisherId;



    public static synchronized ConsentManager getInstance(Context mContext){
        synchronized (ConsentManager.class){
            if (consentManager == null){
                return consentManager = new ConsentManager(mContext);
            }
        }
        return consentManager;
    }

    public ConsentManager(Context mContext) {
        this.mContext = mContext;
        this.sharedPreferences = initPreferences(mContext);
        this.consentPreferences = new ConsentPreferences(mContext);
    }

    // Initialize debug
    public ConsentManager(Context mContext, String publisherId, String privacyURL, boolean DEBUG) {
        this.mContext = mContext;
        this.sharedPreferences = initPreferences(mContext);
        this.publisherId = publisherId;
        this.privacyUrl = privacyURL;
        this.consentManager = this;
    }


    // Initialize production
    public ConsentManager(Context mContext, String publisherId, String privacyURL) {
        this.mContext = mContext;
        this.sharedPreferences = mContext.getSharedPreferences(this.preferences_name, Context.MODE_PRIVATE);
        this.publisherId = publisherId;
        this.privacyUrl = privacyURL;
        this.consentManager = this;
    }

    // Builder class
    public static class Builder {
        private Context mContext;
        private String  LOG_TAG = "ID_LOG";
        private String  DEVICE_ID = "";
        private String  privacyUrl;
        private String  publisherId;
        private String  mMaxAdContentRating;
        private boolean isTagForUnderAgeOfConsent;
        private boolean isTagForChildDirectedTreatment;
        private boolean isDebugGeography;
        private ConsentManager consentManager;

        // Initialize Builder
        public Builder context(Context ctx) {
            this.mContext = ctx;
            return this;
        }

        // Add test device id
        public Builder setTestDeviceId(String val) {
            this.DEVICE_ID = val;
            return this;
        }

        // Add privacy policy
        public Builder setPrivacyUrl(String val) {
            this.privacyUrl = val;
            return this;
        }

        // Add Publisher Id
        public Builder setPublisherId(String val) {
            this.publisherId = val;
            return this;
        }

        public Builder setDebugGeography(boolean val){
            this.isDebugGeography = val;
            return this;
        }

        // Add Logcat id
        public Builder setLog(String val) {
            this.LOG_TAG = val;
            return this;
        }

        public Builder setMaxAdContentRating(@ContentRating.Rating String val){
            this.mMaxAdContentRating = val;
            return this;
        }

        public Builder setTagForUnderAgeOfConsent(boolean val){
            this.isTagForUnderAgeOfConsent = val;
            return this;
        }

        public Builder setTagForChildDirectedTreatment(boolean val){
            this.isTagForChildDirectedTreatment = val;
            return this;
        }

        // Build
        public ConsentManager build() {
            this.consentManager = new ConsentManager(mContext);
            this.consentManager.setPrivacyUrl(privacyUrl);
            this.consentManager.setPublisherId(publisherId);
            this.consentManager.setDebugGeographyEea(isDebugGeography);
            this.consentManager.setTestDeviceId(DEVICE_ID);
            this.consentManager.setLogTag(LOG_TAG);
            this.consentManager.setMaxAdContentRating(mMaxAdContentRating);
            this.consentManager.setTagForUnderAgeOfConsent(isTagForUnderAgeOfConsent);
            this.consentManager.setTagForChildDirectedTreatment(isTagForChildDirectedTreatment);
            return this.consentManager;
        }
    }

    public String getMaxAdContentRating() {
        return this.maxAdContentRating;
    }

    public void setMaxAdContentRating(String val) {
        this.maxAdContentRating = val;
    }

    public boolean isTagForUnderAgeOfConsent() {
        return this.tagForUnderAgeOfConsent;
    }

    public void setTagForUnderAgeOfConsent(boolean val) {
        this.tagForUnderAgeOfConsent = val;
    }

    public boolean isTagForChildDirectedTreatment() {
        return this.tagForChildDirecredTreatment;
    }

    public void setTagForChildDirectedTreatment(boolean val) {
        this.tagForChildDirecredTreatment = val;
    }

    // Initialize SharedPreferences
    private SharedPreferences initPreferences(Context context) {
        return context.getSharedPreferences(this.preferences_name, Context.MODE_PRIVATE);
    }

    // ConsentManager status
    public boolean isConsentPersonalized(Context context) {
        SharedPreferences sharedPreferences = initPreferences(context);
        return sharedPreferences.getBoolean(this.ads_preference, this.IS_PERSONALIZED);
    }

    // ConsentManager is personalized
    private void consentIsPersonalized() {
        sharedPreferences.edit().putBoolean(this.ads_preference, this.IS_PERSONALIZED).apply();
    }

    // ConsentManager is non personalized
    private void consentIsNonPersonalized() {
        sharedPreferences.edit().putBoolean(this.ads_preference, this.NON_PERSONALIZED).apply();
    }

    // ConsentManager is within
    private void updateUserStatus(boolean status) {
        sharedPreferences.edit().putBoolean(this.user_status, status).apply();
    }

    // Get AdRequest
    public AdRequest getAdRequest(Context context) {
        if(isConsentPersonalized(context)) {
            return new AdRequest.Builder().build();
        } else {
            return new AdRequest.Builder()
                    .addNetworkExtrasBundle(AdMobAdapter.class, getNonPersonalizedAdsBundle())
                    .build();
        }
    }

    public boolean isUserFromEea(){
        if(isConsentPersonalized(mContext)) {
            return true;
        } else {
            return false;
        }
    }

    // Get Non Personalized Ads Bundle
    private static Bundle getNonPersonalizedAdsBundle() {
        Bundle extras = new Bundle();
        extras.putString("npa", "1");
        return extras;
    }

    public String getPrivacyUrl() {
        return this.privacyUrl;
    }

    public void setPrivacyUrl(String val) {
        this.privacyUrl = val;
    }

    public String getPublisherId() {
        return this.publisherId;
    }

    public void setPublisherId(String val) {
        this.publisherId = val;
    }

    public boolean isDebugGeographyEea() {
        return this.IS_DEBUG_GEOGRAPHY;
    }

    public void setDebugGeographyEea(boolean val) {
        this.IS_DEBUG_GEOGRAPHY = val;
    }

    public String getTestDeviceId() {
        return this.testDeviceId;
    }

    public void setTestDeviceId(String val) {
        this.testDeviceId = val;
    }

    public String getLogTag() {
        return this.logTag;
    }

    public void setLogTag(String val) {
        this.logTag = val;
    }

    // ConsentManager information
    private void initConsentInformation(final ConsentInformationCallback callback) {
        final ConsentInformation consentInformation = ConsentInformation.getInstance(mContext);
        if (isDebugGeographyEea()) {
            consentInformation.addTestDevice(getTestDeviceId());
            consentInformation.setDebugGeography(DebugGeography.DEBUG_GEOGRAPHY_EEA);
        }
        String[] publisherIds = {this.publisherId};
        consentInformation.requestConsentInfoUpdate(publisherIds, new ConsentInfoUpdateListener() {
            @Override
            public void onConsentInfoUpdated(ConsentStatus consentStatus) {
                if(callback != null) {
                    callback.onResult(consentInformation, consentStatus);
                }
            }

            @Override
            public void onFailedToUpdateConsentInfo(String reason) {
                callback.onFailed(consentInformation, reason);
            }
        });
    }

    // Check if the location is EEA
    public void isRequestLocationIsEeaOrUnknown(final LocationIsEeaOrUnknownCallback callback) {
        // Get ConsentManager information
        initConsentInformation(new ConsentInformationCallback() {
            @Override
            public void onResult(ConsentInformation consentInformation, ConsentStatus consentStatus) {
                callback.onResult(consentInformation.isRequestLocationInEeaOrUnknown());
            }

            @Override
            public void onFailed(ConsentInformation consentInformation, String reason) {
                callback.onResult(false);
            }
        });
    }

    // Check the user location
    public boolean isUserLocationWithinEea(Context mContext) {
        return initPreferences(mContext).getBoolean(this.user_status, false);
    }

    // Initialize ConsentManager SDK
    public void checkLocation(final ConsentCallback callback) {
        // Initialize consentManager information
        initConsentInformation(new ConsentInformationCallback() {
            @Override
            public void onResult(ConsentInformation consentInformation, ConsentStatus consentStatus) {
                // Switch consentManager
                switch(consentStatus) {
                    case UNKNOWN:
                        // Debugging
                        if (isDebugGeographyEea()) {
                            Log.d(getLogTag(), "Unknown ConsentManager");
                            Log.d(getLogTag(), "User location within EEA: " + consentInformation.isRequestLocationInEeaOrUnknown());
                        }
                        // Check the user status
                        if(consentInformation.isRequestLocationInEeaOrUnknown()) {
                            Log.d(getLogTag(), "consentInformation.isRequestLocationInEeaOrUnknown()");
                            request(new ConsentStatusCallback() {
                                @Override
                                public void onResult(boolean isRequestLocationInEeaOrUnknown, int isConsentPersonalized) {
                                    callback.onRequest(isRequestLocationInEeaOrUnknown);
                                }
                            }, new ConsentCallback() {

                                @Override
                                public void onRequest(boolean isRequestLocationInEeaOrUnknown) {
                                    super.onRequest(isRequestLocationInEeaOrUnknown);
                                }

                                @Override
                                public void onResult(boolean isRequestLocationInEeaOrUnknown) {
                                    callback.onResult(isRequestLocationInEeaOrUnknown);
                                }

                                @Override
                                public void onResult(boolean isRequestLocationInEeaOrUnknown, ConsentStatus consentStatus) {
                                    ConsentStatus consentStatusValue;
                                    switch (consentStatus){
                                        case NON_PERSONALIZED:
                                            consentStatusValue = ConsentStatus.NON_PERSONALIZED;
                                            setConsentFormClosed(ConsentFormClosedStatus.IS_FORM_CLICKED_NON_PERSONALIZED);
                                            break;
                                        case PERSONALIZED:
                                            consentStatusValue = ConsentStatus.PERSONALIZED;
                                            setConsentFormClosed(ConsentFormClosedStatus.IS_FORM_CLICKED_PERSONALIZED);
                                            break;
                                        default:
                                            consentStatusValue = ConsentStatus.UNKNOWN;
                                            setConsentFormClosed(ConsentFormClosedStatus.IS_FORM_NON_CLICKED_ANYTHINGS);
                                            break;
                                    }
                                    callback.onResult(isRequestLocationInEeaOrUnknown, consentStatusValue);
                                }
                            });
                        } else {
                            consentIsPersonalized();
                            // Callback
                            callback.onResult(consentInformation.isRequestLocationInEeaOrUnknown());
                            callback.onResult(consentInformation.isRequestLocationInEeaOrUnknown(), consentStatus);
                        }
                        break;
                    case NON_PERSONALIZED:
                        consentIsNonPersonalized();
                        setConsentFormClosed(ConsentFormClosedStatus.IS_FORM_CLICKED_NON_PERSONALIZED);
                        // Callback
                        callback.onResult(consentInformation.isRequestLocationInEeaOrUnknown());
                        callback.onResult(consentInformation.isRequestLocationInEeaOrUnknown(), consentStatus);
                        break;
                    case PERSONALIZED:
                        setConsentFormClosed(ConsentFormClosedStatus.IS_FORM_CLICKED_PERSONALIZED);
                        consentIsPersonalized();
                        // Callback
                        callback.onResult(consentInformation.isRequestLocationInEeaOrUnknown());
                        callback.onResult(consentInformation.isRequestLocationInEeaOrUnknown(), consentStatus);
                        break;
                }
                // Update user status
                updateUserStatus(consentInformation.isRequestLocationInEeaOrUnknown());
            }

            @Override
            public void onFailed(ConsentInformation consentInformation, String reason) {
                if(isDebugGeographyEea()) {
                    Log.d(getLogTag(), "Failed to update: $reason");
                }
                // Update user status
                updateUserStatus(consentInformation.isRequestLocationInEeaOrUnknown());
                // Callback
                callback.onResult(consentInformation.isRequestLocationInEeaOrUnknown());
                callback.onResult(consentInformation.isRequestLocationInEeaOrUnknown(), consentInformation.getConsentStatus());
            }
        });
    }

    // Request ConsentManager
    public void request(final ConsentStatusCallback callback, final ConsentCallback consentCallback) {
        URL privacyUrl = null;
        try {
            privacyUrl = new URL(getPrivacyUrl());
        } catch (MalformedURLException e) {
//            e.printStackTrace();
        }
        form = new ConsentForm.Builder(mContext, privacyUrl)
                .withListener(new ConsentFormListener() {
                    @Override
                    public void onConsentFormLoaded() {
                        if(isDebugGeographyEea()) {
                            Log.d(getLogTag(), "ConsentManager Form is loaded!");
                        }
                        form.show();
                    }

                    @Override
                    public void onConsentFormError(String reason) {
                        if(isDebugGeographyEea()) {
                            Log.d(getLogTag(), "ConsentManager Form ERROR: $reason" + reason);
                        }
                        // Callback on Error
                        if (callback != null) {
                            isRequestLocationIsEeaOrUnknown(new LocationIsEeaOrUnknownCallback() {
                                @Override
                                public void onResult(boolean isRequestLocationInEeaOrUnknown) {
                                    callback.onResult(isRequestLocationInEeaOrUnknown, -1);
                                }
                            });
                        }
                    }

                    @Override
                    public void onConsentFormOpened() {
                        if(isDebugGeographyEea()) {
                            Log.d(getLogTag(), "ConsentManager Form is opened!");
                        }
                    }

                    @Override
                    public void onConsentFormClosed(final ConsentStatus consentStatus, Boolean userPrefersAdFree) {
                        if(isDebugGeographyEea()) {
                            Log.d(getLogTag(), "ConsentManager Form Closed!");
                        }
                        final int isConsentPersonalized;
                        // Check the consentManager status and save it
                        switch (consentStatus) {
                            case NON_PERSONALIZED:
                                consentIsNonPersonalized();
                                isConsentPersonalized = 0;
                                setConsentFormClosed(ConsentFormClosedStatus.IS_FORM_CLICKED_NON_PERSONALIZED);
                                break;
                            case PERSONALIZED:
                                consentIsPersonalized();
                                isConsentPersonalized = 1;
                                setConsentFormClosed(ConsentFormClosedStatus.IS_FORM_CLICKED_PERSONALIZED);
                                break;
                            default:
                                consentIsPersonalized();
                                isConsentPersonalized = 2;
                                setConsentFormClosed(ConsentFormClosedStatus.IS_FORM_NON_CLICKED_ANYTHINGS);
                                break;
                        }
                        // Callback
                        if(callback != null) {
                            isRequestLocationIsEeaOrUnknown(new LocationIsEeaOrUnknownCallback() {
                                @Override
                                public void onResult(boolean isRequestLocationInEeaOrUnknown) {
                                    callback.onResult(isRequestLocationInEeaOrUnknown, isConsentPersonalized);
                                    consentCallback.onResult(isRequestLocationInEeaOrUnknown, consentStatus);
                                    Log.d(getLogTag(), "ConsentManager Form onResult!");
                                }
                            });
                        }
                    }
                })
                .withPersonalizedAdsOption()
                .withNonPersonalizedAdsOption()
                .build();
        form.load();
    }


    public void setConsentFormClosed(ConsentFormClosedStatus consentFormClosed){
        this.sharedPreferences.edit().putString(this.form_closed_preference, consentFormClosed.toString()).apply();
        //Toast.makeText(mContext, consentFormClosed.toString(), Toast.LENGTH_LONG).show();
    }


    public boolean isConsentFormClosed(){
        String equal= this.sharedPreferences.getString(this.form_closed_preference, this.nothins);
        if (equal.equals(ConsentFormClosedStatus.IS_FORM_CLICKED_NON_PERSONALIZED.toString()) || equal.equals(ConsentFormClosedStatus.IS_FORM_CLICKED_PERSONALIZED.toString())){
            return true;
        }
        return false;
    }
}
