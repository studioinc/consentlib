package com.google.ads.consent.listener.abstracts;

public abstract class LocationIsEeaOrUnknownCallback {
    public abstract void onResult(boolean isRequestLocationInEeaOrUnknown);
}
