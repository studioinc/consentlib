package com.google.ads.consent.listener.interfaces;

import com.google.android.gms.ads.reward.RewardItem;

public interface AdRewardedListener {
    void onRewardedVideoAdLoaded();

    void onRewardedVideoAdOpened();

    void onRewardedVideoStarted();

    void onRewardedVideoAdClosed();

    void onRewarded(RewardItem var1);

    void onRewardedVideoAdLeftApplication();

    void onRewardedVideoAdFailedToLoad(int var1);

    void onRewardedVideoCompleted();
}
