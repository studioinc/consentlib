package com.google.ads.consent.listener.abstracts;

import com.google.ads.consent.ConsentStatus;

public interface ConsentListenerCallback {
    void setResult();
    void setResultWithStatus(ConsentStatus consentStatus);
}
