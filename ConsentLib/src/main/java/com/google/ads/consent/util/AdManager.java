package com.google.ads.consent.util;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.google.ads.consent.listener.enums.ContentRating;
import com.google.ads.consent.listener.abstracts.CustomAdListener;
import com.google.ads.consent.listener.enums.CustomAdSize;
import com.google.ads.consent.listener.enums.CustomContentRating;
import com.google.ads.consent.listener.interfaces.AdRewardedListener;
import com.google.ads.consent.ui.ConsentDialog;
import com.google.ads.consent.ui.ConsentManager;
import com.google.ads.mediation.admob.AdMobAdapter;
import com.google.android.ads.consentManager.BuildConfig;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.reward.RewardItem;
import com.google.android.gms.ads.reward.RewardedVideoAd;
import com.google.android.gms.ads.reward.RewardedVideoAdListener;

public class AdManager {
    public Context ctx;
    public ConsentPreferences consentPreferences;
    public SharedPreferences sharedPreferences;
    public static String ads_preference = "ads_preference";
    public static String form_closed_preference = "consent_form_closed_preference";
    public static String preferences_name = "preferences_name";
    public static String user_status = "user_status";
    public static String nothins = "nothins_value";

    public static boolean IS_PERSONALIZED = true;
    public static boolean NON_PERSONALIZED = false;

    public InterstitialAd mInterstitialAd;
    public RewardedVideoAd mRewardedVideoAd;
    public AdView mAdView;
    public AdSize mBannerSize;

    public AdRewardedListener adRewardedListener;
    public CustomAdListener customAdListener;

    public String  isMaxAdContentRating;
    public boolean isTagForUnderAgeOfConsent;
    public boolean isTagForChildDirectedTreatment;


    public static String adTestAppUnitId = "ca-app-pub-3940256099942544~3347511713";
    public static String adTestBannerId = "ca-app-pub-3940256099942544/6300978111";
    public static String adTestIntetstitiaId = "ca-app-pub-3940256099942544/1033173712";
    public static String adTestRewardedVideoId = "ca-app-pub-3940256099942544/5224354917";


    public static int adCountFullScreen = 0;
    public static int adCountRewardedVideo = 0;


    public static String adAppUnitId;
    public static String adBannerId;
    public static String adIntetstitiaId;
    public static String adRewardedVideoId;

    private boolean isDebugMode = false;


    public AdManager(ConsentManager manager) {
        this.ctx = manager.mContext;
        this.IS_PERSONALIZED = manager.IS_PERSONALIZED;
        this.NON_PERSONALIZED = manager.NON_PERSONALIZED;
        this.sharedPreferences = manager.sharedPreferences;
        this.consentPreferences = manager.consentPreferences;
        this.ads_preference = manager.ads_preference;
        this.mBannerSize = AdSize.BANNER;
        this.isMaxAdContentRating = manager.getMaxAdContentRating();
        this.isTagForUnderAgeOfConsent = manager.isTagForUnderAgeOfConsent();
        this.isTagForChildDirectedTreatment = manager.isTagForChildDirectedTreatment();
    }

    public AdManager(ConsentManager manager, boolean debugMode) {
        this.ctx = manager.mContext;
        this.IS_PERSONALIZED = manager.IS_PERSONALIZED;
        this.NON_PERSONALIZED = manager.NON_PERSONALIZED;
        this.sharedPreferences = manager.sharedPreferences;
        this.consentPreferences = manager.consentPreferences;
        this.ads_preference = manager.ads_preference;
        this.mBannerSize = AdSize.BANNER;
        this.isMaxAdContentRating = manager.getMaxAdContentRating();
        this.isTagForUnderAgeOfConsent = manager.isTagForUnderAgeOfConsent();
        this.isTagForChildDirectedTreatment = manager.isTagForChildDirectedTreatment();
        this.isDebugMode = debugMode;
    }

    public AdManager(ConsentDialog dialog) {
        this.ctx = dialog.mContext;
        this.IS_PERSONALIZED = dialog.IS_PERSONALIZED;
        this.NON_PERSONALIZED = dialog.NON_PERSONALIZED;
        this.sharedPreferences = dialog.sharedPreferences;
        this.consentPreferences = dialog.consentPreferences;
        this.ads_preference = dialog.ads_preference;
        this.mBannerSize = AdSize.BANNER;
        this.isMaxAdContentRating = dialog.getMaxAdContentRating();
        this.isTagForUnderAgeOfConsent = dialog.isTagForUnderAgeOfConsent();
        this.isTagForChildDirectedTreatment = dialog.isTagForChildDirectedTreatment();
    }

    public AdManager(ConsentDialog dialog, boolean debugMode) {
        this.ctx = dialog.mContext;
        this.IS_PERSONALIZED = dialog.IS_PERSONALIZED;
        this.NON_PERSONALIZED = dialog.NON_PERSONALIZED;
        this.sharedPreferences = dialog.sharedPreferences;
        this.consentPreferences = dialog.consentPreferences;
        this.ads_preference = dialog.ads_preference;
        this.mBannerSize = AdSize.BANNER;
        this.isMaxAdContentRating = dialog.getMaxAdContentRating();
        this.isTagForUnderAgeOfConsent = dialog.isTagForUnderAgeOfConsent();
        this.isTagForChildDirectedTreatment = dialog.isTagForChildDirectedTreatment();
        this.isDebugMode = debugMode;
    }

    // Initialize Ad App
    public void setAdAppId(String id){
        this.adAppUnitId = id;
        MobileAds.initialize(this.ctx, this.getAdAppUnitId());
    }

    // Initialize interstitial
    public void setAdInterstitialId(String id){
        this.adIntetstitiaId = id;
        this.mInterstitialAd = new InterstitialAd(this.ctx);
        this.mInterstitialAd.setAdUnitId(getAdIntetstitiaId());
    }

    public void loadAdInterstitial(){
        this.mInterstitialAd.loadAd(this.getAdRequest());
        this.mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                if (mInterstitialAd.isLoaded()) {
                    mInterstitialAd.show();
                }
            }
        });
    }

    public void loadAdInterstitial(CustomAdListener callback){
        this.mInterstitialAd.loadAd(this.getAdRequest());
        final CustomAdListener listener = callback;
        this.mInterstitialAd.setAdListener(new AdListener(){
            @Override
            public void onAdClosed() {
                super.onAdClosed();
                if (listener != null){
                    listener.onAdClosed();
                }
            }

            @Override
            public void onAdFailedToLoad(int i) {
                super.onAdFailedToLoad(i);
                if (listener != null){
                    listener.onAdFailedToLoad(i);
                }
            }

            @Override
            public void onAdLeftApplication() {
                super.onAdLeftApplication();
                if (listener != null){
                    listener.onAdLeftApplication();
                }
            }

            @Override
            public void onAdOpened() {
                super.onAdOpened();
                if (listener != null){
                    listener.onAdOpened();
                }
            }

            @Override
            public void onAdLoaded() {
                super.onAdLoaded();
                if (listener != null){
                    listener.onAdLoaded();
                }
            }

            @Override
            public void onAdClicked() {
                super.onAdClicked();
                if (listener != null){
                    listener.onAdClicked();
                }
            }

            @Override
            public void onAdImpression() {
                super.onAdImpression();
                if (listener != null){
                    listener.onAdImpression();
                }
            }
        });
    }

    // Initialize rewarded video
    public void setAdRewardedVideoId(String id){
        this.mRewardedVideoAd = MobileAds.getRewardedVideoAdInstance(this.ctx);
        this.adRewardedVideoId = id;
    }

    public void loadAdRewardedVideo(){
        this.mRewardedVideoAd.loadAd(this.getAdRewardedVideoId(), this.getAdRequest());
        this.mRewardedVideoAd.setRewardedVideoAdListener(new RewardedVideoAdListener() {
            @Override
            public void onRewardedVideoAdLoaded() {
                if (mRewardedVideoAd.isLoaded()){
                    mRewardedVideoAd.show();
                }
                if (adRewardedListener != null){
                    adRewardedListener.onRewardedVideoAdLoaded();
                }
            }

            @Override
            public void onRewardedVideoAdOpened() {
                if (adRewardedListener != null){
                    adRewardedListener.onRewardedVideoAdOpened();
                }
            }

            @Override
            public void onRewardedVideoStarted() {
                if (adRewardedListener != null){
                    adRewardedListener.onRewardedVideoStarted();
                }
            }

            @Override
            public void onRewardedVideoAdClosed() {
                if (adRewardedListener != null){
                    adRewardedListener.onRewardedVideoAdClosed();
                }
            }

            @Override
            public void onRewarded(RewardItem rewardItem) {
                if (adRewardedListener != null){
                    adRewardedListener.onRewarded(rewardItem);
                }
            }

            @Override
            public void onRewardedVideoAdLeftApplication() {
                if (adRewardedListener != null){
                    adRewardedListener.onRewardedVideoAdLeftApplication();
                }
            }

            @Override
            public void onRewardedVideoAdFailedToLoad(int i) {
                if (adRewardedListener != null){
                    adRewardedListener.onRewardedVideoAdFailedToLoad(i);
                }
            }

            @Override
            public void onRewardedVideoCompleted() {
                if (adRewardedListener != null){
                    adRewardedListener.onRewardedVideoCompleted();
                }
            }
        });
    }

    public void loadAdRewardedVideo(AdRewardedListener callback){
        this.mRewardedVideoAd.loadAd(this.getAdRewardedVideoId(), this.getAdRequest());
        final AdRewardedListener listener = callback;
        this.mRewardedVideoAd.setRewardedVideoAdListener(new RewardedVideoAdListener() {
            @Override
            public void onRewardedVideoAdLoaded() {
                if (mRewardedVideoAd.isLoaded()){
                    mRewardedVideoAd.show();
                }
                if (listener != null){
                    listener.onRewardedVideoAdLoaded();
                }
            }

            @Override
            public void onRewardedVideoAdOpened() {
                if (listener != null){
                    listener.onRewardedVideoAdOpened();
                }
            }

            @Override
            public void onRewardedVideoStarted() {
                if (listener != null){
                    listener.onRewardedVideoStarted();
                }
            }

            @Override
            public void onRewardedVideoAdClosed() {
                if (listener != null){
                    listener.onRewardedVideoAdClosed();
                }
            }

            @Override
            public void onRewarded(RewardItem rewardItem) {
                if (listener != null){
                    listener.onRewarded(rewardItem);
                }
            }

            @Override
            public void onRewardedVideoAdLeftApplication() {
                if (listener != null){
                    listener.onRewardedVideoAdLeftApplication();
                }
            }

            @Override
            public void onRewardedVideoAdFailedToLoad(int i) {
                if (listener != null){
                    listener.onRewardedVideoAdFailedToLoad(i);
                }
            }

            @Override
            public void onRewardedVideoCompleted() {
                if (listener != null){
                    listener.onRewardedVideoCompleted();
                }
            }
        });
    }

    public void setAdViewId(String id){
        this.adBannerId = id;
    }

    public void loadAdView(com.google.ads.consent.widget.CustomAdView customAdView){
        this.mAdView = new AdView(this.ctx);
        this.mAdView.setAdSize(this.getBannerSize());
        this.mAdView.setAdUnitId(this.getAdBannerId());
        this.mAdView.loadAd(this.getAdRequest());
        customAdView.addView(this.mAdView, 0);
    }

    public void loadAdView(com.google.ads.consent.widget.CustomAdView customAdView, CustomAdListener listener){
        this.mAdView = new AdView(this.ctx);
        this.mAdView.setAdSize(this.getBannerSize());
        this.mAdView.setAdUnitId(this.getAdBannerId());
        this.mAdView.loadAd(this.getAdRequest());
        customAdView.addView(this.mAdView, 0);
        final CustomAdListener customAdListener = listener;
        this.mAdView.setAdListener(new AdListener() {

            @Override
            public void onAdLoaded() {
                super.onAdLoaded();
                if (customAdListener != null){
                    customAdListener.onAdLoaded();
                }
            }

            @Override
            public void onAdOpened() {
                super.onAdOpened();
                if (customAdListener != null){
                    customAdListener.onAdOpened();
                }
            }

            @Override
            public void onAdClicked() {
                super.onAdClicked();
                if (customAdListener != null){
                    customAdListener.onAdClicked();
                }
            }

            @Override
            public void onAdClosed() {
                super.onAdClosed();
                if (customAdListener != null){
                    customAdListener.onAdClosed();
                }
            }



            @Override
            public void onAdImpression() {
                super.onAdImpression();
                if (customAdListener != null){
                    customAdListener.onAdImpression();
                }
            }

            @Override
            public void onAdLeftApplication() {
                super.onAdLeftApplication();
                if (customAdListener != null){
                    customAdListener.onAdLeftApplication();
                }
            }

            @Override
            public void onAdFailedToLoad(int i) {
                super.onAdFailedToLoad(i);
                if (customAdListener != null){
                    customAdListener.onAdFailedToLoad(i);
                }
            }
        });
    }

    public void loadAdView(AdView adView, CustomAdListener callback){
        mAdView = new AdView(ctx);
        mAdView.setAdSize(getBannerSize());
        mAdView.setAdUnitId(getAdBannerId());
        mAdView.loadAd(getAdRequest());
        adView.addView(mAdView);
        final CustomAdListener listener = callback;
        mAdView.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {
                super.onAdClosed();
                if (listener != null){
                    listener.onAdClosed();
                }
            }

            @Override
            public void onAdFailedToLoad(int i) {
                super.onAdFailedToLoad(i);
                mAdView.setVisibility(View.GONE);
                if (listener != null){
                    listener.onAdFailedToLoad(i);
                }
            }

            @Override
            public void onAdLeftApplication() {
                super.onAdLeftApplication();
                if (listener != null){
                    listener.onAdLeftApplication();
                }
            }

            @Override
            public void onAdOpened() {
                super.onAdOpened();
                if (listener != null){
                    listener.onAdOpened();
                }

            }

            @Override
            public void onAdLoaded() {
                super.onAdLoaded();
                mAdView.setVisibility(View.VISIBLE);
                if (listener != null){
                    listener.onAdLoaded();
                }
            }

            @Override
            public void onAdClicked() {
                super.onAdClicked();
                if (listener != null){
                    listener.onAdClicked();
                }
            }

            @Override
            public void onAdImpression() {
                super.onAdImpression();
                if (listener != null){
                    listener.onAdImpression();
                }
            }
        });
    }

    public void loadAdInterstitialAfterCountOf(int count){
        if (CheckNetwork.getInstance(this.ctx).isOnline()) {
            this.consentPreferences.setAdCountPref("interstitial", Integer.valueOf(this.doAdCounAdFullScreen()));
            if (this.consentPreferences.getAdCountPref("interstitial") == count) {
                this.consentPreferences.setAdCountPref("interstitial", 0);
                this.loadAdInterstitial();
                this.adCountFullScreen = 0;
            }
        }
    }

    public void loadAdRewardedVideoAfterCountOf(int count){
        if (CheckNetwork.getInstance(this.ctx).isOnline()) {
            this.consentPreferences.setAdCountPref("rewarded_video", Integer.valueOf(this.doAdCounRewardedVideo()));
            if (this.consentPreferences.getAdCountPref("rewarded_video") == count) {
                this.consentPreferences.setAdCountPref("rewarded_video", 0);
                this.loadAdRewardedVideo();
                this.adCountRewardedVideo = 0;
            }
        }
    }

    public void setAdRewardedListener(AdRewardedListener listener){
        this.adRewardedListener = listener;
    }

    public void setAdInterstitialListener(CustomAdListener listener){
        this.customAdListener = listener;
    }

    public void setAdViewListener(CustomAdListener listener){
        this.customAdListener = listener;
    }

    public String getAdAppUnitId() {
        if (this.isDebugMode){
            return this.adTestAppUnitId;
        } else {
            if (this.adAppUnitId != null) {
                return this.adAppUnitId;
            } else {
                return this.adTestAppUnitId;
            }
        }
    }

    public void setAdAppUnitId(String mAdAppUnitId) {
        this.adAppUnitId = mAdAppUnitId;
    }

    public String getAdBannerId() {
        if (this.isDebugMode){
            return this.adTestBannerId;
        } else {
            if (this.adBannerId != null) {
                return this.adBannerId;
            } else {
                return this.adTestBannerId;
            }
        }
    }

    public void setAdBannerId(String mAdBannerId) {
        this.adBannerId = mAdBannerId;
    }

    public String getAdIntetstitiaId() {
        if (this.isDebugMode){
            return this.adTestIntetstitiaId;
        } else {
            if (this.adIntetstitiaId != null) {
                return this.adIntetstitiaId;
            } else {
                return this.adTestIntetstitiaId;
            }
        }
    }

    public void setAdIntetstitiaId(String mAdIntetstitiaId) {
        this.adIntetstitiaId = mAdIntetstitiaId;
    }

    public String getAdRewardedVideoId() {
        if (this.isDebugMode){
            return this.adTestRewardedVideoId;
        } else {
            if (this.adRewardedVideoId != null) {
                return this.adRewardedVideoId;
            } else {
                return this.adTestRewardedVideoId;
            }
        }
    }

    public void setAdRewardedId(String mAdRewardedVideoId) {
        this.adRewardedVideoId = mAdRewardedVideoId;
    }

    public String doAdCounAdFullScreen(){
        this.adCountFullScreen++;
        return String.valueOf(this.adCountFullScreen);
    }

    public String doAdCounRewardedVideo(){
        this.adCountRewardedVideo++;
        return String.valueOf(this.adCountRewardedVideo);
    }

    public AdSize getBannerSize() {
        return mBannerSize;
    }

    public void setBannerSize(AdSize mBannerSize) {
        this.mBannerSize = mBannerSize;
    }

    public void setAdViewSize(CustomAdSize customAdSize){
        switch (customAdSize){
            case BANNER:
                setBannerSize(AdSize.BANNER);
                break;
            case FULL_BANNER:
                setBannerSize(AdSize.FULL_BANNER);
            case LARGE_BANNER:
                setBannerSize(AdSize.LARGE_BANNER);
                break;
            case MEDIUM_RECTANGLE:
                setBannerSize(AdSize.MEDIUM_RECTANGLE);
                break;
            case SMART_BANNER:
                setBannerSize(AdSize.SMART_BANNER);
                break;
            default:
                setBannerSize(AdSize.BANNER);
                break;
        }
    }

    public boolean isConsentPersonalized(){
        return this.sharedPreferences.getBoolean(ads_preference, IS_PERSONALIZED);
    }

    public AdRequest getAdRequest(){
        if (this.isConsentPersonalized()){
            Bundle exrtas = new Bundle();
            if (this.isMaxAdContentRating == null && !this.isTagForUnderAgeOfConsent && !this.isTagForChildDirectedTreatment){
                return new AdRequest.Builder().addTestDevice(AdRequest.DEVICE_ID_EMULATOR).build();
            }
            if (this.isMaxAdContentRating != null){
                exrtas.putString("max_ad_content_rating", this.isMaxAdContentRating);
            }
            if (this.isTagForUnderAgeOfConsent){
                exrtas.putBoolean("tag_for_under_age_of_consent", this.isTagForUnderAgeOfConsent);
            }
            if (this.isTagForChildDirectedTreatment){
                return new AdRequest.Builder().addTestDevice(AdRequest.DEVICE_ID_EMULATOR).setIsDesignedForFamilies(this.isTagForChildDirectedTreatment).addNetworkExtrasBundle(AdMobAdapter.class, exrtas).build();
            }
            return new AdRequest.Builder().addTestDevice(AdRequest.DEVICE_ID_EMULATOR).addNetworkExtrasBundle(AdMobAdapter.class, exrtas).build();
        } else {
            Bundle exrtas = new Bundle();
            exrtas.putString("npa", "1");
            if (this.isMaxAdContentRating != null) {
                exrtas.putString("max_ad_content_rating", this.isMaxAdContentRating);
            }
            if (this.isTagForUnderAgeOfConsent){
                exrtas.putBoolean("tag_for_under_age_of_consent", this.isTagForUnderAgeOfConsent);
            }
            if (this.isTagForChildDirectedTreatment){
                return new AdRequest.Builder().addTestDevice(AdRequest.DEVICE_ID_EMULATOR).setIsDesignedForFamilies(this.isTagForChildDirectedTreatment).addNetworkExtrasBundle(AdMobAdapter.class, exrtas).build();
            }
            return new AdRequest.Builder().addTestDevice(AdRequest.DEVICE_ID_EMULATOR).addNetworkExtrasBundle(AdMobAdapter.class, exrtas).build();
        }
    }

    public void resume(){
        if (this.mAdView != null){
            this.mAdView.resume();
        }
    }

    public void pause(){
        if (this.mAdView != null){
            this.mAdView.pause();
        }
    }

    public void destroy(){
        if (this.mAdView != null){
            this.mAdView.destroy();
        }
    }
}