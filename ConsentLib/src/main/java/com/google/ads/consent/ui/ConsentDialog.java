package com.google.ads.consent.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.ads.consent.AdProvider;
import com.google.ads.consent.listener.enums.CustomContentRating;
import com.google.ads.consent.ConsentInformation;
import com.google.ads.consent.util.ConsentPreferences;
import com.google.ads.consent.ConsentStatus;
import com.google.ads.consent.DebugGeography;
import com.google.ads.consent.util.LogDebug;
import com.google.ads.consent.listener.enums.ConsentFormClosedStatus;
import com.google.ads.consent.listener.abstracts.ConsentListenerCallback;
import com.google.ads.consent.listener.interfaces.ConsentInfoUpdateListener;
import com.google.ads.consent.util.TranslucentDialog;
import com.google.android.ads.consentManager.R;
import com.google.android.gms.ads.AdRequest;

import java.util.List;

public class ConsentDialog {
    public static Context mContext;
    public ConsentPreferences consentPreferences;
    public static SharedPreferences sharedPreferences;

    public String  APP_NAME = "Google Admob ConsentSDK";
    public String  PRIVACY_URL = "https://policies.google.com/privacy";
    public String  PUBLISHER_ID = "pub-0123456789012345";
    public boolean DEBUG_GEOGRAPHY = false;
    public boolean DIALOG_FULL_SCREEN = false;

    public String  maxAdContentRating;
    public boolean tagForUnderAgeOfConsent;
    public boolean tagForChildDirecredTreatment;

    public static String adTestAppUnitId = "ca-app-pub-3940256099942544~3347511713";
    public static String adTestBannerId = "ca-app-pub-3940256099942544/6300978111";
    public static String adTestIntetstitiaId = "ca-app-pub-3940256099942544/1033173712";
    public static String adTestRewardedVideoId = "ca-app-pub-3940256099942544/5224354917";

    public static String ads_preference = "ads_preference";
    public static String form_closed_preference = "consent_form_closed_preference";
    public static String preferences_name = "preferences_name";
    public static String user_status = "user_status";
    public static String nothins = "nothins_value";


    public static boolean IS_PERSONALIZED = true;
    public static boolean NON_PERSONALIZED = false;
    public static boolean IS_DEBUG_GEOGRAPHY = false;

    public static boolean isAdsPersonalize;
    public static boolean isAdsNonPersonalize;
    public static boolean isUserNotFromEeaPersonalize;


    public static final int AdsNonPersonalize = 0;
    public static final int AdsPersonalize = 1;
    public static final int AdsUserNotFromEeaPersonalize = 2;


    public static String testDeviceId = "";
    public static String logTag = "";
    public static int adCountFullScreen = 0;
    public static int adCountRewardedVideo = 0;


    public static String adAppUnitId;
    public static String adBannerId;
    public static String adIntetstitiaId;
    public static String adRewardedVideoId;

    public static String privacyUrl;
    public static String publisherId;


    public ConsentListenerCallback consentCallback;

    public static ConsentDialog consentDialog;
    //public static AdManager mAdManager;


    public void setConsentCallback(ConsentListenerCallback callback){
        this.consentCallback = callback;
    }

    public ConsentDialog(Context context) {
        this.mContext = context;
        this.consentPreferences =  new ConsentPreferences(mContext);
        this.sharedPreferences =  initPreferences(mContext);
        //this.mAdManager = new AdManager();
    }

    public static synchronized ConsentDialog getInstance(Context mContext){
        synchronized (ConsentDialog.class){
            if (consentDialog == null){
                return new ConsentDialog(mContext);
            }
        }
        return consentDialog;
    }


    public SharedPreferences initPreferences(Context mContext){
        return mContext.getSharedPreferences(this.ads_preference, Context.MODE_PRIVATE);
    }

    // Initialized Builder Class
    public static class Builder {
        private ConsentDialog mConsentDialog;
        private Context mContext;
        private String  mAppName;
        private String  mPrivacyUrl;
        private String  mPublisherId;
        private String  mMaxAdContentRating;
        private boolean isTagForUnderAgeOfConsent;
        private boolean isTagForChildDirectedTreatment;
        private boolean isDebugGeography;
        private boolean mDialogFullscreen;
        private ConsentListenerCallback mCallback;

        public Builder context(Context context){
            this.mContext = context;
            return this;
        }

        public Builder setAppName(String val){
            this.mAppName = val;
            return this;
        }

        public Builder setPrivacyUrl(String val){
            this.mPrivacyUrl = val;
            return this;
        }

        public Builder setPublisherId(String val){
            this.mPublisherId = val;
            return this;
        }

        public Builder setDebugGeography(boolean val){
            this.isDebugGeography = val;
            return this;
        }

        public Builder setMaxAdContentRating(String val){
            this.mMaxAdContentRating = val;
            return this;
        }

        public Builder setTagForUnderAgeOfConsent(boolean val){
            this.isTagForUnderAgeOfConsent = val;
            return this;
        }

        public Builder setTagForChildDirectedTreatment(boolean val){
            this.isTagForChildDirectedTreatment = val;
            return this;
        }

        public Builder setDialogFullscreen(boolean val){
            this.mDialogFullscreen = val;
            return this;
        }

        // listener of Class Builder
        public Builder listener(ConsentListenerCallback callback){
            this.mCallback = callback;
            return this;
        }

        // Build of Class Builder
        public ConsentDialog build(){
            this.mConsentDialog = new ConsentDialog(mContext);
            this.mConsentDialog.setAppName(mAppName);
            this.mConsentDialog.setPrivacyUrl(mPrivacyUrl);
            this.mConsentDialog.setPublisherId(mPublisherId);
            this.mConsentDialog.setDebugGeography(isDebugGeography);
            this.mConsentDialog.setConsentCallback(mCallback);
            this.mConsentDialog.setDialogFullscreen(mDialogFullscreen);
            this.mConsentDialog.setMaxAdContentRating(mMaxAdContentRating);
            this.mConsentDialog.setTagForUnderAgeOfConsent(isTagForUnderAgeOfConsent);
            this.mConsentDialog.setTagForChildDirectedTreatment(isTagForChildDirectedTreatment);
            return this.mConsentDialog;
        }

    }

    public String getMaxAdContentRating() {
        return this.maxAdContentRating;
    }

    public void setMaxAdContentRating(String max_ad_content_rating) {
        this.maxAdContentRating = max_ad_content_rating;
    }

    public boolean isTagForUnderAgeOfConsent() {
        return this.tagForUnderAgeOfConsent;
    }

    public void setTagForUnderAgeOfConsent(boolean tag_for_under_age_of_consent) {
        this.tagForUnderAgeOfConsent = tag_for_under_age_of_consent;
    }

    public boolean isTagForChildDirectedTreatment() {
        return this.tagForChildDirecredTreatment;
    }

    public void setTagForChildDirectedTreatment(boolean tag_for_child_directed_treatment) {
        this.tagForChildDirecredTreatment = tag_for_child_directed_treatment;
    }

    public void show(){
        checkConsentStatus();
    }

    // https://developers.google.com/admob/android/eu-consent
    public void checkConsentStatus(final ConsentListenerCallback callback){
        ConsentInformation consentInformation = ConsentInformation.getInstance(mContext);
        if (isDebugGeography()){
            Toast.makeText(mContext, isDebugGeography()+"", Toast.LENGTH_LONG).show();
            consentInformation.addTestDevice(AdRequest.DEVICE_ID_EMULATOR); // enter your device id, if you need it for testing
            consentInformation.setDebugGeography(DebugGeography.DEBUG_GEOGRAPHY_EEA);
        }
        String[] publisherIds = {getPublisherId()}; // enter your admob pub-id
        consentInformation.requestConsentInfoUpdate(publisherIds, new ConsentInfoUpdateListener() {
            @Override
            public void onConsentInfoUpdated(ConsentStatus consentStatus) {
                //User's consent status successfully updated: + consentStatus
                LogDebug.d(mContext.getClass().getName(), "User's consent status successfully updated: " +consentStatus);


                if (ConsentInformation.getInstance(mContext).isRequestLocationInEeaOrUnknown()){
                    // User is from EU
                    LogDebug.d(mContext.getClass().getName(), "User is from EU");

                    /////////////////////////////
                    // TESTING - reset the choice
                    //ConsentInformation.getInstance(mContext).setConsentStatus(ConsentStatus.UNKNOWN);
                    /////////////////////////////

                    // If the returned ConsentStatus is UNKNOWN, collect user's consent.
                    if (consentStatus == ConsentStatus.UNKNOWN) {
                        showConsentDialog(false, callback);
                    }

                    // If the returned ConsentStatus is PERSONALIZED or NON_PERSONALIZED
                    // the user has already provided consent. Forward consent to the Google Mobile Ads SDK.
                    else if (consentStatus == ConsentStatus.NON_PERSONALIZED) {
                        consentIsPersonalezid(NON_PERSONALIZED);
                        callback.setResult();
                        callback.setResultWithStatus(ConsentStatus.NON_PERSONALIZED);
                        //mShowNonPersonalizedAdRequests = true;

                        // The default behavior of the Google Mobile Ads SDK is to serve personalized ads.
                        // If a user has consented to receive only non-personalized ads, you can configure
                        // an AdRequest object with the following code to specify that only non-personalized
                        // ads should be returned.

                    } else if (consentStatus == ConsentStatus.PERSONALIZED){
                        consentIsPersonalezid(IS_PERSONALIZED);
                        callback.setResult();
                        callback.setResultWithStatus(ConsentStatus.PERSONALIZED);
                    }

                } else {
                    //log("User is NOT from EU");
                    // we don't have to do anything
                }

            }

            @Override
            public void onFailedToUpdateConsentInfo(String errorDescription) {
                //User's consent status failed to update: + errorDescription
                LogDebug.d(mContext.getClass().getName(), "User's consent status failed to update: " + errorDescription);
            }
        });
    }

    // https://developers.google.com/admob/android/eu-consent
    public void checkConsentStatus(){
        ConsentInformation consentInformation = ConsentInformation.getInstance(mContext);
        if (isDebugGeography()){
            LogDebug.d("isDebugGeography",
                    " ====> " + isDebugGeography()+"");
            consentInformation.addTestDevice(AdRequest.DEVICE_ID_EMULATOR); // enter your device id, if you need it for testing
            consentInformation.setDebugGeography(DebugGeography.DEBUG_GEOGRAPHY_EEA);
        }
        String[] publisherIds = {getPublisherId()}; // enter your admob pub-id
        consentInformation.requestConsentInfoUpdate(publisherIds, new ConsentInfoUpdateListener() {
            @Override
            public void onConsentInfoUpdated(ConsentStatus consentStatus) {
                // User's consent status successfully updated: + consentStatus
                LogDebug.d(mContext.getClass().getName(), "User's consent status successfully updated: " +consentStatus);


                if (ConsentInformation.getInstance(mContext).isRequestLocationInEeaOrUnknown()){
                    // User is from EU
                    LogDebug.d(mContext.getClass().getName(), "User is from EU");

                    /////////////////////////////
                    // TESTING - reset the choice
                    //ConsentInformation.getInstance(mContext).setConsentStatus(ConsentStatus.UNKNOWN);
                    /////////////////////////////

                    // If the returned ConsentStatus is UNKNOWN, collect user's consent.
                    if (consentStatus == ConsentStatus.UNKNOWN) {
                        showConsentDialog(false, consentCallback);
                    }

                    // If the returned ConsentStatus is PERSONALIZED or NON_PERSONALIZED
                    // the user has already provided consent. Forward consent to the Google Mobile Ads SDK.
                    else if (consentStatus == ConsentStatus.NON_PERSONALIZED) {
                        consentIsPersonalezid(NON_PERSONALIZED);
                        setConsentFormClosed(ConsentFormClosedStatus.IS_FORM_CLICKED_NON_PERSONALIZED);
                        if (consentCallback != null){
                            consentCallback.setResult();
                            consentCallback.setResultWithStatus(ConsentStatus.NON_PERSONALIZED);
                        }

                        // The default behavior of the Google Mobile Ads SDK is to serve personalized ads.
                        // If a user has consented to receive only non-personalized ads, you can configure
                        // an AdRequest object with the following code to specify that only non-personalized
                        // ads should be returned.

                    } else if (consentStatus == ConsentStatus.PERSONALIZED){
                        consentIsPersonalezid(IS_PERSONALIZED);
                        setConsentFormClosed(ConsentFormClosedStatus.IS_FORM_CLICKED_PERSONALIZED);
                        if (consentCallback != null){
                            consentCallback.setResult();
                            consentCallback.setResultWithStatus(ConsentStatus.PERSONALIZED);
                        }
                    }

                } else {
                    // User is NOT from EU
                    // we don't have to do anything
                    setConsentFormClosed(ConsentFormClosedStatus.IS_FORM_NON_CLICKED_ANYTHINGS);
                    LogDebug.d(mContext.getClass().getName(), "User is NOT from EU");
                }

            }

            @Override
            public void onFailedToUpdateConsentInfo(String errorDescription) {
                // User's consent status failed to update: + errorDescription
                LogDebug.d(mContext.getClass().getName(), "User's consent status failed to update: " + errorDescription);
                setConsentFormClosed(ConsentFormClosedStatus.IS_FORM_CLICKED_PERSONALIZED);
                if (consentCallback != null){
                    consentCallback.setResult();
                    consentCallback.setResultWithStatus(ConsentStatus.PERSONALIZED);
                }
            }
        });
    }

    public boolean isConsentPersonalized(){
        return this.sharedPreferences.getBoolean(this.user_status, IS_PERSONALIZED);
    }
    public void consentIsPersonalezid(boolean bool){
        this.sharedPreferences.edit().putBoolean(this.user_status, bool).apply();
    }

    /**
     *
     * for (Map.Entry<String, String> entry : getNetworkExtrasBundle().entrySet()) {
     * exrtas.putString(entry.getKey(), entry.getValue());
     * }
     * @return
     */

    public boolean checkConsentForAds() {
        final ConsentInformation consentInformation = ConsentInformation.getInstance(mContext);
        if (!consentInformation.isRequestLocationInEeaOrUnknown()) {
            // User is outside EEA
            LogDebug.d(mContext.getClass().getName(), "User is outside EEA");
            return true;
        }

        final ConsentStatus consentStatus = consentInformation.getConsentStatus();
        // ConstentStatus: + consentStatus
        LogDebug.d(mContext.getClass().getName(), "ConstentStatus: " + consentStatus);

        if (ConsentStatus.UNKNOWN.equals(consentStatus)) {
            // Need to ask for consent
            LogDebug.d(mContext.getClass().getName(), "Need to ask for consent");
            // we need to ask for consent
            return false;
        }

        return true;
    }

    public void showConsentDialog(boolean showCancel, final ConsentListenerCallback callback) {
        final TranslucentDialog builder;
        if (isDialogFullscreen()){
            builder = new TranslucentDialog(mContext, R.style.DialogTheme);
        } else {
            builder = new TranslucentDialog(mContext, 0);
        }
        builder.requestWindowFeature(Window.FEATURE_NO_TITLE);
        //AlertDialog.Builder alertDialog = new AlertDialog.Builder(new ContextThemeWrapper(mContext,  R.style.full_screen_dialog));
        Activity activity = (Activity) mContext;
        LayoutInflater inflater = activity.getLayoutInflater();
        View eu_consent_dialog = inflater.inflate(R.layout.eu_consent, null);
        //alertDialog.setView(eu_consent_dialog).setCancelable(false);
        builder.setCancelable(showCancel);
        builder.setContentView(eu_consent_dialog);

        //if (showCancel) alertDialog.setPositiveButton(R.string.dialog_close, null);
        //mEuDialog = alertDialog.create();
        builder.show();

        Button btn_eu_consent_yes = eu_consent_dialog.findViewById(R.id.btn_eu_consent_yes);
        Button btn_eu_consent_no = eu_consent_dialog.findViewById(R.id.btn_eu_consent_no);
        Button btn_eu_consent_remove_ads = eu_consent_dialog.findViewById(R.id.btn_eu_consent_remove_ads);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
            btn_eu_consent_yes.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.button_background));
        } else {
            //btn_eu_consent_yes.setBackgroundColor(mContext.getResources().getColor(R.color.green_light));
            //btn_eu_consent_yes.setTextColor(mContext.getResources().getColor(android.R.color.white));

            ContextThemeWrapper newContext = new ContextThemeWrapper(mContext, R.style.RedButtonLightTheme);
            //button = new Button(newContext);
            //Paris.style(btn_eu_consent_remove_ads).apply(R.style.RedButtonLightTheme);

           /* StyleHolder<Button> styleHolder = new ButtonStyleHolder(mContext);
            styleHolder.applyStyle(btn_eu_consent_remove_ads);*/
        }


        btn_eu_consent_yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (builder.isShowing()) {
                    builder.cancel();
                    builder.dismiss();
                }
                LogDebug.d(mContext.getClass().getName(), mContext.getResources().getString(R.string.thank_you));
                ConsentInformation.getInstance(mContext).setConsentStatus(ConsentStatus.PERSONALIZED);
                consentIsPersonalezid(IS_PERSONALIZED);
                setConsentFormClosed(ConsentFormClosedStatus.IS_FORM_CLICKED_PERSONALIZED);
                if (callback != null){
                    callback.setResult();
                    callback.setResultWithStatus(ConsentStatus.PERSONALIZED);
                }
            }
        });
        btn_eu_consent_no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (builder.isShowing()) {
                    builder.cancel();
                    builder.dismiss();
                }
                LogDebug.d(mContext.getClass().getName(), mContext.getResources().getString(R.string.thank_you));
                ConsentInformation.getInstance(mContext).setConsentStatus(ConsentStatus.NON_PERSONALIZED);
                consentIsPersonalezid(NON_PERSONALIZED);
                setConsentFormClosed(ConsentFormClosedStatus.IS_FORM_CLICKED_NON_PERSONALIZED);
                if (callback != null){
                    callback.setResult();
                    callback.setResultWithStatus(ConsentStatus.NON_PERSONALIZED);
                }
            }
        });
        btn_eu_consent_remove_ads.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (builder.isShowing()) {
                    builder.cancel();
                    builder.dismiss();
                }
                //IAP_buyAdsFree(); // YOUR REMOVE ADS METHOD
            }
        });

        TextView tv_eu_learn_more = eu_consent_dialog.findViewById(R.id.tv_eu_learn_more);
        tv_eu_learn_more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                euMoreInfoDialog();
            }
        });
    }

    private void euMoreInfoDialog(){
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        ScrollView sv = new ScrollView(mContext);
        LinearLayout ll = new LinearLayout(mContext);
        ll.setOrientation(LinearLayout.VERTICAL);

        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        params.setMargins(40, 20, 40, 20);

        TextView tv_my_privacy_policy = new TextView(mContext);
        tv_my_privacy_policy.setTextSize(12);
        String link = "<a href="+getPrivacyUrl()+">"+getAppName()+"</a>";
        tv_my_privacy_policy.setText(Html.fromHtml(link));
        tv_my_privacy_policy.setMovementMethod(LinkMovementMethod.getInstance());
        ll.addView(tv_my_privacy_policy, params);

        TextView tv_google_partners = new TextView(mContext);
        tv_google_partners.setText(R.string.google_partners);
        tv_google_partners.setPadding(40,40,40,20);
        ll.addView(tv_google_partners);

        List<AdProvider> adProviders = ConsentInformation.getInstance(mContext).getAdProviders();
        for (AdProvider adProvider : adProviders) {
            //log("adProvider: " +adProvider.getName()+ " " +adProvider.getPrivacyPolicyUrlString());
            link = "<a href="+adProvider.getPrivacyPolicyUrlString()+">"+adProvider.getName()+"</a>";
            TextView tv_adprovider = new TextView(mContext);
            tv_adprovider.setTextSize(12);
            tv_adprovider.setText(Html.fromHtml(link));
            tv_adprovider.setMovementMethod(LinkMovementMethod.getInstance());
            ll.addView(tv_adprovider, params);
        }
        sv.addView(ll);

        builder.setTitle(R.string.privacy_policy).setView(sv).setPositiveButton(R.string.dialog_close, null);

        final AlertDialog createDialog = builder.create();
        createDialog.show();

    }

    public void setConsentFormClosed(ConsentFormClosedStatus consentFormClosed){
        this.sharedPreferences.edit().putString(form_closed_preference, consentFormClosed.toString()).apply();
        //Toast.makeText(mContext, consentFormClosed.toString(), Toast.LENGTH_LONG).show();
    }

    public boolean isConsentFormClosed(){
        String equal= this.sharedPreferences.getString(form_closed_preference, nothins);
        if (equal.equals(ConsentFormClosedStatus.IS_FORM_CLICKED_NON_PERSONALIZED.toString()) || equal.equals(ConsentFormClosedStatus.IS_FORM_CLICKED_PERSONALIZED.toString())){
            return true;
        }
        return false;
    }

    public String getAppName() {
        return APP_NAME;
    }

    public void setAppName(String APP_NAME) {
        this.APP_NAME = APP_NAME;
    }

    public String getPrivacyUrl() {
        return PRIVACY_URL;
    }

    public void setPrivacyUrl(String PRIVACY_URL) {
        this.PRIVACY_URL = PRIVACY_URL;
    }

    public String getPublisherId() {
        return PUBLISHER_ID;
    }

    public void setPublisherId(String PUBLISHER_ID) {
        this.PUBLISHER_ID = PUBLISHER_ID;
    }

    public boolean isDebugGeography() {
        return DEBUG_GEOGRAPHY;
    }

    public void setDebugGeography(boolean DEBUG_GEOGRAPHY) {
        this.DEBUG_GEOGRAPHY = DEBUG_GEOGRAPHY;
    }

    public boolean isDialogFullscreen() {
        return DIALOG_FULL_SCREEN;
    }

    public void setDialogFullscreen(boolean DIALOG_FULL_SCREEN) {
        this.DIALOG_FULL_SCREEN = DIALOG_FULL_SCREEN;
    }
}
