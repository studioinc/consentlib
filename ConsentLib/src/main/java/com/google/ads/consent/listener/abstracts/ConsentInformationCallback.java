package com.google.ads.consent.listener.abstracts;

import com.google.ads.consent.ConsentInformation;
import com.google.ads.consent.ConsentStatus;

public abstract class ConsentInformationCallback {
    public abstract void onResult(ConsentInformation consentInformation, ConsentStatus consentStatus);
    public abstract void onFailed(ConsentInformation consentInformation, String reason);
}
