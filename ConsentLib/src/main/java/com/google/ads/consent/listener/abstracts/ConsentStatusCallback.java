package com.google.ads.consent.listener.abstracts;

public abstract class ConsentStatusCallback {
    abstract public void onResult(boolean isRequestLocationInEeaOrUnknown, int isConsentPersonalized);
}