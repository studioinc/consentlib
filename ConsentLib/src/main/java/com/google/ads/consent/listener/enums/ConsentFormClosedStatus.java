package com.google.ads.consent.listener.enums;

public enum ConsentFormClosedStatus {
    IS_FORM_CLICKED_NON_PERSONALIZED,
    IS_FORM_CLICKED_PERSONALIZED,
    IS_FORM_NON_CLICKED_ANYTHINGS,
    SET_RESET_CONSENT_FORM
}
