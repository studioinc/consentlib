package com.google.ads.consent.annotations;

import android.support.annotation.IntDef;
import android.support.annotation.StringDef;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public abstract class ContentRating {

    // Define the list of accepted constants and declare the ContentRating annotation
    @Retention(RetentionPolicy.SOURCE)
    @StringDef({G, PG, T, MA})
    public @interface Rating{}

    // Define the list of accepted constants and declare the ContentRating annotation
    @Retention(RetentionPolicy.SOURCE)
    @IntDef({IS_TARGETING, IS_MAX_AD_CONTENT_RATING, IS_DESIGNED_FOR_FAMILIES, TAG_FOR_UNDER_AGE_OF_CONSENT})
    public @interface Mode{}

    // Declare the constants
    public static final String G = "G";
    public static final String PG = "PG";
    public static final String T = "T";
    public static final String MA = "MA";


    public static final int IS_TARGETING = 0;
    public static final int IS_MAX_AD_CONTENT_RATING = 1;
    public static final int IS_DESIGNED_FOR_FAMILIES = 2;
    public static final int TAG_FOR_UNDER_AGE_OF_CONSENT = 3;

    // Attach the annotation
    public abstract void setContentRating(@Rating String val);

    // Decorate the target methods with the annotation
    @Rating
    public abstract String getContentRating();

    // Decorate the target methods with the annotation
    @Mode
    public abstract int getContentRatingMode();

    // Attach the annotation
    public abstract void setContentRatingMode(@Mode int val);


}
