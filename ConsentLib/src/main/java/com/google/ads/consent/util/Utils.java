package com.google.ads.consent.util;

public class Utils {

    public static boolean checkLinkExt(String url){
        if (url.startsWith("http://")){
            return true;
        } else if (url.startsWith("https://")){
            return true;
        } else {
            return false;
        }
    }

    public static String stripNonDigits(final CharSequence input){
        final StringBuilder sb = new StringBuilder(input.length());
        for(int i = 0; i < input.length(); i++){
            final char c = input.charAt(i);
            if(c > 47 && c < 58){
                sb.append(c);
            }
        }
        return sb.toString();
    }
}
