package com.google.ads.consent.listener.enums;

public enum ContentRating {
    G,
    PG,
    T,
    MA
}
