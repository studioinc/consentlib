package com.google.ads.consent.listener.enums;

public enum CustomContentRating {
    IS_TARGETING,
    IS_MAX_AD_CONTENT_RATING,
    IS_DESIGNED_FOR_FAMILIES,
    TAG_FOR_UNDER_AGE_OF_CONSENT,
}
