package com.google.ads.consent.listener.abstracts;

import com.google.ads.consent.ConsentStatus;

public abstract class ConsentCallback {
    public void onRequest(boolean isRequestLocationInEeaOrUnknown){}
    public void onResult(boolean isRequestLocationInEeaOrUnknown){}
    public void onResult(boolean isRequestLocationInEeaOrUnknown, ConsentStatus consentStatus){}
}
