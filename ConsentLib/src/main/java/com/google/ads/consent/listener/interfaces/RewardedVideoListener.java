package com.google.ads.consent.listener.interfaces;

import com.google.android.gms.ads.reward.RewardItem;

//TODO: Rewarded Video Callback result
public interface RewardedVideoListener {
    void onRewardedVideoAdLoaded();
    void onRewardedVideoAdOpened();
    void onRewardedVideoAdStarted();
    void onRewardedAd(RewardItem rewardItem);
    void onRewardedVideoAdClosed();
    void onRewardedVideoAdLeftApplication();
    void onRewardedVideoAdFailedToLoad(int i);
    void onRewardedVideoAdCompleted();
}