package com.google.ads.consent.listener.enums;

public enum CustomAdSize {
    BANNER,
    FULL_BANNER,
    LARGE_BANNER,
    MEDIUM_RECTANGLE,
    SMART_BANNER,
}
